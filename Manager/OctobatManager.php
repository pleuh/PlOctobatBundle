<?php


namespace Pl\OctobatBundle\Manager;

use Pl\OctobatBundle\Interfaces\OctobatCustomerInterface;


/**
 * Class OctobatManager
 * @package Pl\OctobatBundle\Manager
 * @property string $secretKey
 */
class OctobatManager
{
	private $secretKey;

	/**
	 * OctobatManager constructor.
	 * @param $secretKey
	 */
	public function __construct($secretKey){
		$this->secretKey = $secretKey;
	}

	private const METHOD_PATCH = "PATCH";

	private function makeRequest($url, $type = "GET", $params = []){
		$baseUrl = "https://apiv2.octobat.com/";
		$ch = curl_init($baseUrl . $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_USERPWD, $this->secretKey . ":");

		if(in_array($type, [self::METHOD_PATCH])){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		}
		$rep = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($httpcode == 200){
			return json_decode($rep, true);
		}
		else{
			throw new \RuntimeException("Impossible de récupérer la réponse d'octobat");
		}

		return null;
	}



	public function getCustomer($octobatCustomerId){
		return $this->makeRequest("customers/" . $octobatCustomerId);
	}
	public function updateCustomer($octobatCustomerId, $params){
		return $this->makeRequest("customers/" . $octobatCustomerId, self::METHOD_PATCH, $params);
	}


	public function getOctobatIdFromStripeId(string $stripeId){
		if($rep = $this->makeRequest("customers/")){
			foreach($rep["data"] as $data){
				if(array_key_exists("sources", $data)){
					foreach($data["sources"] as $source){
						if($source["identifier"] == $stripeId){
							return $data["id"];
						}

					}
				}
			}
		}
		return null;
	}

	/**
	 * @param OctobatCustomerInterface $octobatCustomer
	 */
	public function getFacturesForCustomer(OctobatCustomerInterface $octobatCustomer){
		if($octobatCustomer->getOctobatId() == null){
			return [];
		}

		if($rep = $this->makeRequest("invoices?customer=" . $octobatCustomer->getOctobatId())){
			$invoicesIds = array_map(function ($data){
				return [
					"id" => $data["id"],
					"uri" => sprintf("https://repo.octobat.com/invoices/%s.pdf", $data["id"]),
					"amount" => $data["total_gross_amount"] / 100,
					"date" => date_create_from_format("Y-m-d", $data["invoice_date"]),
				];
			}, $rep["data"]);


			return $invoicesIds;
		}

		return null;
	}


}
