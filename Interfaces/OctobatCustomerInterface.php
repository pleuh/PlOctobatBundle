<?php

namespace Pl\OctobatBundle\Interfaces;

/**
 * Interface OctobatCustomerInterface
 * @package Pl\StripeBundle\Interfaces
 */
interface OctobatCustomerInterface{

    /**
     * @return string
     */
    public function getOctobatId();

	/**
     * @param string $octobatId
     * @return mixed
     */
    public function setOctobatId($octobatId);


}